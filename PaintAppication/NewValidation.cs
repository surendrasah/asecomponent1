using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PaintAppication
{
    /// <summary>
    /// This class is created to perform validation for all the commands in the second part.
    /// Its main function is to check the validation and report to the user before executing the commands
    /// </summary>
    public class NewValidation
    {

        private TextBox txtCommand;
        public Boolean isValidCommand = true;
        public Boolean isSomethingInvalid = false;

        public int lineNumber = 0;


        String[] variable = { "radius", "width", "height", "repeat", "value", "breadth", "perpendicular", "theight", "value1", "value2", "randomValue" };
        String[] shapes = { "circle", "rectangle", "triangle", "drawto", "moveto" };


        public int loopLineNo = 0, endLoopLineNo = 0, ifLineNo = 0, endIfLineNo = 0, methodLineNo = 0, pMethodLineNo = 0, endMethodLineNo = 0, endPMethodLineNo = 0;


        public Boolean isMethodDefined = false;
        public Boolean isPMethodDefined = false;
        bool endPMethodFound = false;
        bool hasPMethod = false;
        int numberOfLines = 0;

        /// <summary>
        /// Boolean to check if loopblock is inside ifblock
        /// </summary>
        public bool ifhasLoop;

        /// <summary>
        /// Boolean to check if loopblock contains if block
        /// </summary>
        public bool LoophasIf;

        /// <summary>
        /// This is the main method of this class, which sends each command to check for validation. It first checks if there is parameterized method, 
        /// if so then it sends the method for syntax checking in the checkParameterizedMethod() method, else, it sends the commands to 
        /// checkLineValidation
        /// </summary>
        /// <param name="txtCommand">The Windows Control Textbox, it takes as a parameter, when this method is called from Form1.cs, the program box is 
        /// passed here</param>
        public void ProgramCommands(TextBox txtCommand)
        {
            this.txtCommand = txtCommand;

            numberOfLines = txtCommand.Lines.Length;
            for (int i = 0; i < numberOfLines; i++)
            {
                String oneLineCommand = txtCommand.Lines[i];
                oneLineCommand = oneLineCommand.Trim();
                if (!oneLineCommand.Equals(""))
                {
                    lineNumber = i + 1;

                    if (oneLineCommand.Contains("method mypmethod(param int[] list)"))//when method defination for parameterized method is found
                    {

                        hasPMethod = true;
                        checkParameterizedMethod();
                    }

                    if (!hasPMethod)
                    {
                        try
                        {
                            checkLineValidation(oneLineCommand);
                        }
                        catch (Exception e)
                        {
                            MessageBox.Show(e.Message);
                        }
                    }

                }


            }

            /*
             * When all validation is done, it finally checks if there are any invalid commands
             */

            if (isValidCommand)
            {
                isSomethingInvalid = false;
            }
            else
            {
                isSomethingInvalid = true;
            }

            this.ifhasLoop = ifContainsLoop();
            this.LoophasIf = LoopContainsIf();

        }

        /// <summary>
        /// Another main method which checks each command line by line. And reports to user if any command is invalid
        /// </summary>
        /// <param name="lineOfCommand">The line of command from the program box.</param>
        public void checkLineValidation(string lineOfCommand)
        {
            ///<summary>
            ///These are the selected keywords which the users can use.
            ///</summary>

            String[] keyword = { "radius", "width", "height", "repeat", "value", "breadth", "perpendicular",
                                "theight", "value1", "value2","if","endif","circle", "rectangle", "triangle",
                                "drawto", "moveto", "loop","endloop" ,"method","endmethod","call","randomValue","endpmethod","reset","clear"};

            lineOfCommand = Regex.Replace(lineOfCommand, @"\s+", " ");
            string[] words = lineOfCommand.Split(' ');
            //removing white spaces in between words
            for (int i = 0; i < words.Length; i++)
            {
                words[i] = words[i].Trim().ToLower();
            }
            //first check if first word is a keywords
            String firstWord = words[0].ToLower();
            Boolean firstWordIsKeyword = keyword.Contains(firstWord);

            if (firstWordIsKeyword) //first checking if a random keyword is used
            {
                Boolean firstWordIsShape = shapes.Contains(words[0].ToLower());
                Boolean firstWordIsVariable = variable.Contains(words[0].ToLower());


                /**
                 * if a shape command is recognized, it checks for validity of parameters, checks if correct parameter variable name is used
                 * checks if all parameters are either integers or variables
                 */
                if (firstWordIsShape)
                {
                    if (words[0].ToLower().Equals("circle"))
                    {
                        if (words.Length == 2)
                        {
                            Boolean isInt = words[1].All(char.IsDigit);
                            if (!isInt)
                            {

                                Boolean isVariable = variable.Contains(words[1].ToLower());
                                if (isVariable)
                                {
                                    if (words[1] == "radius")
                                    {
                                        checkIfVariableDefined(words[1]);
                                    }
                                    else
                                    {
                                        MessageBox.Show("Circle needs radius to draw at line number:" + lineNumber);
                                        isValidCommand = false;
                                    }
                                }
                                else
                                {
                                    MessageBox.Show("Proper variable is not used at line number:" + lineNumber);
                                    isValidCommand = false;
                                }
                            }
                        }
                        else
                        {
                            MessageBox.Show("Invalid number of parameters for circle at line number:" + lineNumber);
                            isValidCommand = false;

                        }
                    }
                    else if (words[0].ToLower().Equals("rectangle"))
                    {
                        String args = lineOfCommand.Substring(9, (lineOfCommand.Length - 9));
                        String[] parms = args.Split(',');

                        if (parms.Length == 2)
                        {
                            bool isBothInt = false;
                            bool isBothVar = false;


                            bool iseitherInt1 = false;
                            bool iseitherInt2 = false;



                            parms[0].Trim().ToLower();
                            parms[1].Trim().ToLower();



                            iseitherInt1 = int.TryParse(parms[0], out _);
                            iseitherInt2 = int.TryParse(parms[1], out _);

                            isBothInt = (iseitherInt1 && iseitherInt2);
                            isBothVar = (!iseitherInt1 && !iseitherInt2);


                            if (isBothInt == false && isBothVar == false) //if both values are neither int nor variables
                            {
                                isValidCommand = false;
                                throw new ParameterException("Either all parameters must be integers or all must be variables! at line number:" + lineNumber);

                            }
                            else
                            {

                                if (isBothVar)
                                {
                                    for (int i = 0; i < parms.Length; i++)//for each parameter
                                    {
                                        bool isVariable = variable.Contains(parms[i].ToLower().Trim());

                                        if (isVariable)
                                        {
                                            if (parms[i].Trim() == "width" || parms[i].Trim() == "height")//there is a space before the first word!
                                            {

                                                checkIfVariableDefined(parms[i].Trim()); //when a shape command has variable, it checks if the variable is defined first
                                            }
                                            else
                                            {
                                                MessageBox.Show("Rectangle needs width and height at line number:" + lineNumber);
                                                isValidCommand = false;
                                            }


                                        }
                                        else
                                        {
                                            MessageBox.Show("Proper variable is not used :: " + parms[i] + " at line number:" + lineNumber);
                                            isValidCommand = false;
                                        }

                                    }
                                }
                            }

                        }
                        else
                        {
                            MessageBox.Show("Parameters for rectangle is invalid at line number:" + lineNumber + 1);
                            isValidCommand = false;
                        }
                    }

                    else if (words[0].ToLower().Equals("triangle"))
                    {
                        String args = lineOfCommand.Substring(8, (lineOfCommand.Length - 8));
                        String[] parms = args.Split(',');

                        if (parms.Length == 3)
                        {

                            bool isAllInt = false;
                            bool isAllVar = false;

                            bool iseitherInt1 = false;
                            bool iseitherInt2 = false;
                            bool iseitherInt3 = false;
                            parms[0].Trim().ToLower();
                            parms[1].Trim().ToLower();
                            parms[2].Trim().ToLower();

                            iseitherInt1 = int.TryParse(parms[0], out _);
                            iseitherInt2 = int.TryParse(parms[1], out _);
                            iseitherInt3 = int.TryParse(parms[2], out _);
                            isAllInt = (iseitherInt1 && iseitherInt2 && iseitherInt3); //checking if all parameters are integers
                            isAllVar = (!iseitherInt1 && !iseitherInt2 && !iseitherInt3); //checking if all parameters are variables

                            if (isAllInt == false && isAllVar == false) //if both values are neither int nor variables
                            {

                                isValidCommand = false;
                                throw new ParameterException("Either all parameters should be Integers, or all should be variables at line number: " + lineNumber);
                            }
                            else
                            {

                                if (isAllVar)
                                {
                                    for (int i = 0; i < parms.Length; i++)//for each parameter
                                    {
                                        bool isVariable = variable.Contains(parms[i].ToLower().Trim());

                                        if (isVariable)
                                        {
                                            if (parms[i].Trim() == "theight" || parms[i].Trim() == "breadth" || parms[i].Trim() == "perpendicular")
                                            {

                                                checkIfVariableDefined(parms[i].Trim()); //when a shape command has variable, it checks if the variable is defined first
                                            }
                                            else
                                            {
                                                MessageBox.Show("Triangle needs HEIGHT and BREADTH and PERPENDICULAR at line number:" + lineNumber);
                                                isValidCommand = false;
                                            }


                                        }
                                        else
                                        {
                                            MessageBox.Show("Proper variable is not used :: " + parms[i] + " at line number:" + lineNumber);
                                            isValidCommand = false;
                                        }

                                    }
                                }
                            }

                        }
                        else
                        {
                            MessageBox.Show("Parameters for triangle is invalid at line number:" + lineNumber + 1);
                            isValidCommand = false;
                        }
                    }
                    else if (words[0].ToLower().Equals("drawto"))
                    {
                        String args = lineOfCommand.Substring(6, (lineOfCommand.Length - 6));
                        String[] parms = args.Split(',');

                        if (parms.Length == 2)
                        {
                            bool isBothInt = false;
                            bool isBothVar = false;

                            bool iseitherInt1 = false;
                            bool iseitherInt2 = false;
                            parms[0].Trim().ToLower();
                            parms[1].Trim().ToLower();


                            iseitherInt1 = int.TryParse(parms[0], out _);
                            iseitherInt2 = int.TryParse(parms[1], out _);
                            isBothInt = (iseitherInt1 && iseitherInt2);
                            isBothVar = (!iseitherInt1 && !iseitherInt2);

                            if (isBothInt == false && isBothVar == false) //if both values are neither int nor variables
                            {

                                isValidCommand = false;
                                throw new ParameterException("Either all parameters must be integers or all must be variables! at line number:" + lineNumber);
                            }
                            else
                            {

                                if (isBothVar)
                                {
                                    for (int i = 0; i < parms.Length; i++)//for each parameter
                                    {
                                        bool isVariable = variable.Contains(parms[i].ToLower().Trim());

                                        if (isVariable)
                                        {
                                            if (parms[i].Trim() == "value1" || parms[i].Trim() == "value2")//there is a space before the first word!
                                            {

                                                checkIfVariableDefined(parms[i].Trim());
                                            }
                                            else
                                            {
                                                MessageBox.Show("drawto command needs value1 and value2 variables at line number:" + lineNumber);
                                                isValidCommand = false;
                                            }


                                        }
                                        else
                                        {
                                            MessageBox.Show("Proper variable is not used :: " + parms[i] + " at line number:" + lineNumber);
                                            isValidCommand = false;
                                        }

                                    }
                                }
                            }

                        }
                        else
                        {
                            MessageBox.Show("Parameters for drawto command is invalid at line number:" + lineNumber + 1);
                            isValidCommand = false;
                        }
                    }
                    else if (words[0].ToLower().Equals("moveto"))
                    {
                        String args = lineOfCommand.Substring(6, (lineOfCommand.Length - 6));
                        String[] parms = args.Split(',');

                        if (parms.Length == 2)
                        {
                            bool isBothInt = false;
                            bool isBothVar = false;

                            bool iseitherInt1 = false;
                            bool iseitherInt2 = false;
                            parms[0].Trim().ToLower();
                            parms[1].Trim().ToLower();


                            iseitherInt1 = int.TryParse(parms[0], out _);
                            iseitherInt2 = int.TryParse(parms[1], out _);
                            isBothInt = (iseitherInt1 && iseitherInt2);
                            isBothVar = (!iseitherInt1 && !iseitherInt2);

                            if (isBothInt == false && isBothVar == false) //if both values are neither int nor variables
                            {
                                isValidCommand = false;
                                throw new ParameterException("Either all parameters must be integers or all must be variables! at line number:" + lineNumber);
                            }
                            else
                            {

                                if (isBothVar)
                                {
                                    for (int i = 0; i < parms.Length; i++)//for each parameter
                                    {
                                        bool isVariable = variable.Contains(parms[i].ToLower().Trim());

                                        if (isVariable)
                                        {
                                            if (parms[i].Trim() == "value1" || parms[i].Trim() == "value2")//there is a space before the first word!
                                            {

                                                checkIfVariableDefined(parms[i].Trim());
                                            }
                                            else
                                            {
                                                MessageBox.Show("moveto command needs value1 and value2 variables at line number:" + lineNumber);
                                                isValidCommand = false;
                                            }


                                        }
                                        else
                                        {
                                            MessageBox.Show("Proper variable is not used :: " + parms[i] + " at line number:" + lineNumber);
                                            isValidCommand = false;
                                        }

                                    }
                                }
                            }

                        }
                        else
                        {
                            MessageBox.Show("Parameters for moveto command is invalid at line number:" + lineNumber + 1);
                            isValidCommand = false;
                        }
                    }
                    else { }

                }

                /**
                 * When first word is variable, it must be variable assignment
                 */
                else if (firstWordIsVariable)
                {

                    if (!(words.Length < 3)) //variable assignment command should have length 3 or more
                    {
                        int k;
                        if (words[1] == "=") //variable assigment must have equal to sign in the second position
                        {
                            if (lineOfCommand.Contains('+'))
                            {
                                if ((words.Length == 5)) //variable adiition must have five words
                                {
                                    int l;
                                    if ((words[3] == "+"))
                                    {
                                        bool isBothInt = false;
                                        bool isBothVar = false;
                                        bool finaldecision = false;


                                        isBothInt = (int.TryParse(words[2], out l) && int.TryParse(words[4], out l));
                                        isBothVar = (!(int.TryParse(words[2], out l)) && !(int.TryParse(words[4], out l)));

                                        finaldecision = (isBothInt || isBothVar); //both should be false
                                        if (finaldecision == false)
                                        {
                                            //the variable should be added to same variable
                                            //find out which one is variable
                                            //by trying to parse each word in the either side of the Plus operator
                                            if (int.TryParse(words[2], out l))
                                            {
                                                if (!(words[4] == words[0]))
                                                {
                                                    MessageBox.Show("New addition should be made to the same variable at line number " + lineNumber);
                                                    isValidCommand = false;
                                                }
                                                //also check if the value to be added is defined first!
                                                else
                                                {
                                                    checkIfVariableDefined(words[4]);
                                                }
                                            }
                                            else //the last word should be Integer then
                                            {
                                                if (!(words[2] == words[0]))
                                                {
                                                    MessageBox.Show("New addition should be made to the same variable at line number " + lineNumber);
                                                    isValidCommand = false;
                                                }
                                                else
                                                {
                                                    checkIfVariableDefined(words[2]); //also check if the value to be added is defined first!
                                                }
                                            }
                                        }
                                        else
                                        {
                                            MessageBox.Show("Variable Addition needs one variable and one Integer at line number " + lineNumber);
                                            isValidCommand = false;
                                        }
                                    }//when it contains plus operation
                                    else
                                    {
                                        MessageBox.Show("Your plus operator for Variable Assignment is not in correct position at line number:" + lineNumber);
                                        isValidCommand = false;
                                    }

                                }
                                else
                                {
                                    MessageBox.Show("Only 5 words are allowed during variable addition at line number " + lineNumber);
                                    isValidCommand = false;
                                }
                            }
                            else
                            {
                                if ((words.Length == 3))
                                {
                                    if (!int.TryParse(words[2], out k)) //During variable assignment, the 3rd word must be integer
                                    {
                                        MessageBox.Show("The variable should be assigned an Integer at line number:" + lineNumber);
                                        isValidCommand = false;
                                    }
                                }
                                else
                                {
                                    MessageBox.Show("Only 3 words are allowed during variable assignment at line number " + lineNumber);
                                    isValidCommand = false;
                                }

                            }
                        }
                        else
                        {
                            MessageBox.Show("A variable should be immediately followed by equal to sign " + lineNumber);
                            isValidCommand = false;
                        }//when an equal to sign is not after variable

                    }//when variable assignment command's length is less
                    else
                    {
                        MessageBox.Show("Variable Assignment is incomplete in line number " + lineNumber);
                        isValidCommand = false;
                    }


                }
                else if (words[0] == "loop") //when the loop keyword is encountered
                {
                    loopLineNo = lineNumber;
                    int q;
                    bool endLoopFound = false;
                    string commandlines = txtCommand.Text;
                    string[] eachline = commandlines.Split('\n');
                    if (words.Length == 2)
                    {
                        if (words[1].Trim().ToLower() != "0")
                        {
                            if (!(int.TryParse(words[1], out q)))// if the second word is not an Integer
                            {
                                if (words[1].Trim().ToLower() == "repeat")
                                {
                                    checkIfVariableDefined("repeat"); //check if repeat variable is defined
                                }//if loop word is not followed by repeat
                                else
                                {
                                    MessageBox.Show("Loop command should be followed by REPEAT keyword at line number " + lineNumber);
                                    isValidCommand = false;
                                }
                            }
                        }
                        else
                        {
                            MessageBox.Show("Loop cannot repeat zero times at line number " + lineNumber);
                            isValidCommand = false;
                        }

                        //now search for endloop keyword

                        if (eachline.Length != 1)
                        {
                            for (int i = 0; i < eachline.Length; i++) //goes through each line and searches for endloop keyword
                            {

                                string s = eachline[i];
                                string[] aWord = s.Split(' ');
                                if (aWord[0].Trim().ToLower() == "endloop")
                                {
                                    endLoopFound = true; //a boolean value is set to true when endLoop is found
                                    endLoopLineNo = i + 1;

                                }
                            }
                            if (loopLineNo > endLoopLineNo)
                            {

                                endLoopFound = false;
                                MessageBox.Show("endloop should be after loop statement");
                                isValidCommand = false;
                            }
                        }
                        else
                        {
                            endLoopFound = false;
                            MessageBox.Show("Loop block should be of more than 1 statement");
                            isValidCommand = false;
                        }
                        if (!endLoopFound)
                        {
                            MessageBox.Show("Define end loop!");
                            isValidCommand = false;
                        }

                    }//loop word should be followed by only one word
                    else
                    {
                        MessageBox.Show("Invalid way to start a loop command at line number" + lineNumber);
                        isValidCommand = false;
                    }
                }
                else if (words[0] == "if") //when the if keyword is found
                {
                    ifLineNo = lineNumber;
                    if ((words.Length == 4)) //first checks the number of words in if condition
                    {
                        if (words[2] == "=" || words[2] == ">" || words[2] == "<") //only 3 operators are allowd for comparing
                        {
                            bool endIfFound = false;
                            string commandlines = txtCommand.Text;
                            string[] eachline = commandlines.Split('\n');
                            int l;
                            bool isBothInt = false;
                            bool isBothVar = false;
                            bool finaldecision = false;

                            //either side of operator, one must be integer and other must be variable
                            isBothInt = (int.TryParse(words[1], out l) && int.TryParse(words[3], out l));
                            isBothVar = (!(int.TryParse(words[1], out l)) && !(int.TryParse(words[3], out l)));

                            finaldecision = (isBothInt || isBothVar); //both should be false

                            if (finaldecision == false)
                            {
                                //find out which one is variable 
                                if (int.TryParse(words[1], out l))
                                {
                                    if (variable.Contains(words[3])) //random words cannot be compared
                                    { checkIfVariableDefined(words[3]); } //the variable with which we are comparing must be defined
                                    else
                                    {
                                        MessageBox.Show("You cannot compare a random word, it should be a selected variable at line number " + lineNumber);
                                        isValidCommand = false;
                                    }
                                }
                                else
                                {
                                    if (variable.Contains(words[1])) //random words cannot be compared
                                    { checkIfVariableDefined(words[1]); } //the variable with which we are comparing must be defined
                                    else
                                    {
                                        MessageBox.Show("You cannot compare a random word, it should be a selected variable at line number " + lineNumber);
                                        isValidCommand = false;
                                    }
                                }

                            }
                            else
                            {
                                MessageBox.Show("While Comparison, one must be Integer, one must be entity at line number " + lineNumber);
                                isValidCommand = false;
                            }

                            //now search for endif keyword

                            if (eachline.Length != 1)
                            {
                                for (int i = 0; i < eachline.Length; i++) //go through each line in the program box to search for endif
                                {

                                    string s = eachline[i];
                                    string[] aWord = s.Split(' ');
                                    if (aWord[0].Trim().ToLower() == "endif")
                                    {
                                        endIfFound = true; //a boolean value is set to true when endif keyword is found
                                        endIfLineNo = i + 1;
                                    }
                                }
                                if (ifLineNo > endIfLineNo)
                                {
                                    endIfFound = false;
                                    MessageBox.Show("endif should be after if statement");
                                    isValidCommand = false;
                                }
                            }
                            else
                            {
                                endIfFound = false;
                                MessageBox.Show("If block should be of more than 1 statement");
                                isValidCommand = false;
                            }
                            if (!endIfFound)
                            {
                                MessageBox.Show("Define endif !");
                            }
                        }//there must be comparing entity in between
                        else
                        {
                            MessageBox.Show("There must be (=) or (>) or (<) sign in between comparing quantities at line number " + lineNumber);
                            isValidCommand = false;
                        }
                    }//if condition must have length equal to 4
                    else
                    {
                        MessageBox.Show("Number of words for if condition is invalid at line number " + lineNumber);
                        isValidCommand = false;
                    }
                }
                else if (words[0].Trim().ToLower() == "method") //when themethod keyword is found
                {
                    methodLineNo = lineNumber;

                    bool endMethodFound = false;

                    string commandlines = txtCommand.Text;
                    string[] eachline = commandlines.Split('\n');
                    if (words[1].Trim().ToLower() == "mymethod") //first checks if the name of method used is a valid word
                    {
                        if (eachline.Length != 1)
                        {
                            for (int i = 0; i < eachline.Length; i++) //then searches  each command for end method
                            {

                                string s = eachline[i];
                                string[] aWord = s.Split(' ');
                                if (aWord[0].Trim().ToLower() == "endmethod")
                                {
                                    endMethodFound = true; //sets a boolean value to true when endmethod is found
                                    endMethodLineNo = i + 1;
                                    isMethodDefined = true;
                                }
                            }
                            if (methodLineNo > endMethodLineNo) //this makes sure that method ending block is after method defination
                            {
                                endMethodFound = false;
                                MessageBox.Show("endmethod should be after method defination at line number:" + lineNumber);
                                isValidCommand = false;
                            }

                            if (!endMethodFound)
                            {
                                MessageBox.Show("Define endmethod for method command! at line number:" + lineNumber);
                                isValidCommand = false;
                            }
                        }
                        else
                        {
                            endMethodFound = false;
                            MessageBox.Show("Method block should be of more than 1 statement at line number:" + lineNumber);
                            isValidCommand = false;
                        }

                    }
                    //mypmethod removed
                    //the method defined is must be  mymethod 
                    else
                    {
                        MessageBox.Show("Unknown method defined at line number:" + lineNumber);
                        isValidCommand = false;
                    }

                }
                else if (words[0].Trim().ToLower() == "call") //when call keyword is found
                {
                    if (words.Length == 2)
                    {

                        int len = words[1].Length;
                        if (len != 8)
                        {
                            isValidCommand = false;
                            throw new MethodNameException("The method you are trying to call doesn't exist"); //uses User defined exception to throw error
                                                                                                              //if the user tries to call some odd method  
                        }


                        if (words[1].Trim().ToLower() == "mymethod")
                        {
                            if (!isMethodDefined)//checks if method is defined before calling
                            {
                                MessageBox.Show("mymethod is not defined to call at line number " + lineNumber);
                                isValidCommand = false;
                            }
                        }

                        else
                        {
                            MessageBox.Show("No such method exists to call at line number:" + lineNumber);
                            isValidCommand = false;
                        }
                    }
                    else
                    {
                        MessageBox.Show("Invalid way to call a method at line number " + lineNumber);
                        isValidCommand = false;
                    }
                }
            }
            else
            {
                MessageBox.Show("Unknown commands/variables at line number " + lineNumber);
                isValidCommand = false;
            }

            if (!isValidCommand)
            {
                // isSomethingInvalid = true;
            }

        }

        /// <summary>
        /// This method checks if a variable is defined before using it.
        /// Goes through each command from the command text and searches if the variable exists before the line number
        /// </summary>
        /// <param name="variable">The variable to be checked</param>
        public void checkIfVariableDefined(string variable)
        {
            Boolean isVariableFound = false;
            if (txtCommand.Lines.Length > 1) //variable declaration and use needs atleast two lines
            {
                for (int i = 0; i < lineNumber - 1; i++)
                {
                    String oneLineCommand = txtCommand.Lines[i];
                    if (oneLineCommand.Contains(variable))
                    {
                        isVariableFound = true; //a boolean value is set to true when variable is found
                        break;
                    }

                }
                if (!isVariableFound)
                {
                    MessageBox.Show(variable + " variable is not defined at line number" + lineNumber);
                    isValidCommand = false;
                }

            }
            else
            {
                MessageBox.Show(variable + "From Outer most loop Variable is not defined at line number" + lineNumber); //this is understood, variable declaration and uses atleast two lines
                isValidCommand = false;
            }
        }

        /// <summary>
        /// This method does the entire validation for parameterized method, including the calling of parameterized method
        /// </summary>
        public void checkParameterizedMethod()
        {
            int hasPCall = 0;
            int minParamRequirement = 2;
            pMethodLineNo = lineNumber;
            string commandlines = txtCommand.Text;
            string[] eachline = commandlines.Split('\n');


            if (eachline.Length != 1)
            {
                pMethodLineNo = lineNumber;


                for (int m = 0; m < eachline.Length; m++)
                {

                    string s = eachline[m];
                    string[] aWord = s.Split(' ');


                    if (aWord[0].Trim().ToLower() == "endpmethod")
                    {
                        endPMethodFound = true; //when endpmethod is found, a boolean value is set to true
                        endPMethodLineNo = m + 1;

                    }
                    if (aWord[0].Trim().ToLower().Contains("triangle"))//checking if there is triangle in command for changing requirement of program
                    {

                        minParamRequirement = 3; //in my program, I have kept a minimum requirement of 2 parameters while calling  parameterized method
                        //if there is triangle, then the minimum requirement is 3
                    }
                    if (aWord[0].Trim().ToLower() == "call")

                    {
                        if (aWord[1].Trim().ToLower().Contains("mypmethod"))
                        {

                            hasPCall = 1;
                            string[] var = aWord[1].Split('('); //now check if parameters are sent or not
                            if (var.Length != 2)
                            {
                                MessageBox.Show("Incorrect syntax for calling parameterized method, bracket should be opened and closed"); //this means parameters are not sent
                                isValidCommand = false;
                            }
                            else //it is confirmed that there is bracket
                            {
                                int len = var[1].Length;
                                if (len >= 2)
                                {
                                    bool isInt = true;
                                    int y;
                                    var[1] = var[1].Replace(")", "");
                                    string[] args = var[1].Split(',');
                                    //after splitting the first value must be integer


                                    for (int i = 0; i < args.Length; i++)
                                    {
                                        isInt = int.TryParse(args[i], out y); //while parsing, check if there is any non-integer
                                        if (!isInt)
                                        {
                                            MessageBox.Show("You can pass only integers while calling method at line number:" + m); //this means parameters sent are not integers
                                            isValidCommand = false;
                                        }
                                    }

                                    if (args.Length < minParamRequirement)//users must pass atleast minimum parameters
                                    {

                                        MessageBox.Show("Please pass the required number of parameters, which is 3 if there is triangle, otherwise 2 at line number:" + m); //this means parameters sent are not integers
                                        isValidCommand = false;
                                    }

                                }
                                else
                                {
                                    MessageBox.Show("Incorrect syntax for calling parameterized method, bracket should be closed and there must be intger at line number:" + m); //this means parameters are not sent
                                    isValidCommand = false;
                                }
                            }


                        }
                        else
                        {
                            MessageBox.Show("unknown method called at line number:" + lineNumber);
                            isValidCommand = false;
                        }
                    }



                }
                if (pMethodLineNo > endPMethodLineNo)
                {
                    endPMethodFound = false;
                    MessageBox.Show("endpmethod should be after method defination at line number:" + lineNumber);
                    isValidCommand = false;
                }

                if (!endPMethodFound)
                {
                    MessageBox.Show("Define endpmethod for method command! at line number:" + lineNumber);
                    isValidCommand = false;
                }


            }
            else
            {
                endPMethodFound = false;
                MessageBox.Show("Method block should be of more than 1 statement at line number:" + lineNumber);
                isValidCommand = false;
            }
            //calling 

            if (hasPCall == 1)//if call mypmethod exists, ignore the that line
            {
                numberOfLines = numberOfLines - 1;
            }

            //by rule, while executing parameterized method, there should not be any other commands
            //now check if there are any commands before and after the parameterized method, if so, give warning message to user
            if ((pMethodLineNo != 1) || (numberOfLines != endPMethodLineNo)) //also check if pmethod is defined
            {
                MessageBox.Show("Do not keep parameterized method with other commands!");
                isValidCommand = false;

            }
            else
            {   //if method defination satisfies all rules then 
                isPMethodDefined = true;
            }

            /**
             * Now check the methods inside the parameterized method
             **/
            if (isPMethodDefined) //if method defination satisfies all rules then only check the commands
            {
                int q;
                bool isInt = false;
                for (int i = lineNumber + 1; i < endPMethodLineNo; i++)//go through each command 
                {

                    string k = eachline[i - 1].Trim().ToLower();
                    string[] aWord = k.Split(' ');
                    for (int j = 0; j < aWord.Length; j++)
                    {
                        aWord[j].Trim().ToLower();
                    }

                    if (aWord.Length == 2)
                    {
                        if (aWord[0] == "circle" || aWord[0] == "rectangle" || aWord[0] == "triangle" || aWord[0] == "drawto" || aWord[0] == "moveto") //commands other than this is not allowd
                        {
                            string[] parameters = aWord[1].Split(',');
                            for (int n = 0; n < parameters.Length; n++)
                            {
                                isInt = int.TryParse(parameters[n], out q);
                                if (isInt)
                                {
                                    MessageBox.Show("You cannot pass  integers to define parameterized method at line number:" + i); //this means parameters sent are not integers
                                    isValidCommand = false;
                                }
                            }
                        }
                        else
                        {
                            MessageBox.Show("Your commands aren't supported in parameterized method at line number:" + i);
                            isValidCommand = false;
                        }
                    }
                    else
                    {
                        MessageBox.Show("Invalid commands kept in parameterized method at line number:" + i);
                        isValidCommand = false;
                    }


                }
            }




        }

        /// <summary>
        /// This method is for the additional functionality. Checks if the loop block is contained inside if block
        /// </summary>
        /// <returns>a boolean value indicating if loop is inside if block</returns>
        public bool ifContainsLoop()
        {

            if ((ifLineNo < loopLineNo) && (endLoopLineNo < endIfLineNo))
            {

                return true;
            }
            else
            {

                return false;
            }
        }

        /// <summary>
        /// This method is for additional functionaliy. Checks if loop block contains if block
        /// </summary>
        /// <returns> a boolean value indicating loop block contains if block</returns>
        public bool LoopContainsIf()
        {
            if ((loopLineNo < ifLineNo) && (endIfLineNo < endLoopLineNo))
            {
                return true;
            }
            else
            {
                return false;
            }
        }



    }
}

