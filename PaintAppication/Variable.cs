using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaintAppication
{
    class Variable
   
       
        {

            /// <summary>
            /// Getter and Setter for string variable
            /// </summary>
            public string variable { get; set; }

            /// <summary>
            /// Getter and setter for float value
            /// </summary>
            public float value { get; set; }

            /// <summary>
            /// Set method of variable which takes variable as a parameter
            /// </summary>
            /// <param name="variable">user uninputted variable name</param>
            public void setVariable(string variable)
            {
                this.variable = variable;
            }

            /// <summary>
            /// Get method of variable
            /// </summary>
            /// <returns>variable name</returns>
            public string getVariable()
            {
                return this.variable;
            }

            /// <summary>
            /// Set method of value which takes value as a parameter
            /// </summary>
            /// <param name="value">user input value of a variable</param>
            public void setValue(float value)
            {
                this.value = value;
            }
            /// <summary>
            /// Get method of value
            /// </summary>
            /// <returns>value of a variable</returns>
            public float getValue()
            {
                return this.value;
            }
        }
    }


