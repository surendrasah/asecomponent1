using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaintAppication
{
    /// <summary>
    /// This is a user-defined exception class, which implements the exception class
    /// This exception is thrown to warn user to give either all integer parameters
    /// or all variable parameters
    /// </summary>
    class ParameterException : Exception
    {
        /// <summary>
        /// The constructor class of exception that shows specified error to the user
        /// </summary>
        /// <param name="message">The exception message to be shown to the user</param>
        public ParameterException(String message) : base(message)
        {

        }
    }
}

