﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaintAppication
{
    /// <summary>
    /// class to determine which factory object to create for testing purpose
    /// </summary>
    public class FactoryProducerCheck
    {
        //create factory of different color and shape
        /// <summary>
        /// checks the factory object
        /// </summary>
        /// <param name="choice">string-type of object to create</param>
        /// <returns>factory object or null based on choice</returns>
        public bool isShape(String shapeType)
        {
            if (shapeType == "shape")
            {
                return true;
            }
            return false;
        }
    }
}
